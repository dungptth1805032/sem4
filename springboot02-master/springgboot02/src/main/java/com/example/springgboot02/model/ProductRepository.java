package com.example.springgboot02.model;

import com.example.springgboot02.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
